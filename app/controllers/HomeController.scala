package controllers

import javax.inject._
import play.api.mvc._
import zadanie1.Node
import zadanie1.Zadanie1.readXlxs

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  val calculatedList: List[Node] = readXlxs("Scala_zadanie.xlsx")


  def index() = Action {
    Ok(views.html.index(calculatedList))
  }

}
