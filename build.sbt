name := "Zadanie2"
 
version := "1.0" 
      
lazy val `zadanie2` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"
      
resolvers += "Akka Snapshot Repository" at "https://repo.akka.io/snapshots/"
      
scalaVersion := "2.12.2"

libraryDependencies ++= Seq( jdbc , ehcache , ws , specs2 % Test , guice )

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest-funsuite" % "3.2.0" % "test",
  "org.scalatest" %% "scalatest-mustmatchers" % "3.2.0" % "test",
  "org.scalatest" %% "scalatest-flatspec" % "3.2.0" % "test",
  "org.scalacheck" %% "scalacheck" % "1.14.3" % "test",
  "org.scalatest" %% "scalatest-propspec" % "3.2.0" % "test",
  "org.apache.poi" % "poi" % "4.1.2",
  "org.apache.poi" % "poi-ooxml" % "4.1.2",
  "com.google.code.gson" % "gson" % "2.8.2",
  "commons-codec" % "commons-codec" % "1.14",
  "org.apache.commons" % "commons-collections4" % "4.4",
  "org.apache.commons" % "commons-math3" % "3.6.1",
  "org.apache.xmlbeans" % "xmlbeans" % "3.1.0"
)

unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

      